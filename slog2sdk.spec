Name:           slog2sdk
Version:        1.2.9
Release:        1%{?dist}
Summary:        provides traceTOslog2 library and graphical viewer jumpshot
License:	MIT
BuildArch:	noarch

URL:            https://gitlab.com/netcoder/slog2sdk
Source0:        https://gitlab.com/netcoder/slog2sdk/-/archive/%{version}/slog2sdk-%{version}.tar.bz2

BuildRequires:  java-17-openjdk-devel
BuildRequires:	make
BuildRequires:	coreutils
BuildRequires:	sed

Requires:       java-17-openjdk

%description
slog2sdk provides traceTOslog2 library and graphical viewer jumpshot.

For more information see:
https://www.mcs.anl.gov/research/projects/perfvis/download/index.htm#slog2sdk

%prep
%setup -q

%build
%set_build_flags
make -O -j1 prefix=%{_prefix} datadir=%{_datadir} all

%install
%make_install prefix=%{_prefix} datadir=%{_datadir} install

%files
%license COPYRIGHT
%{_bindir}/jumpshot
%{_datadir}/java/jumpshot-%{version}.jar
%{_datadir}/java/jumpshot.jar
%{_datadir}/java/traceTOslog2-%{version}.jar
%{_datadir}/java/traceTOslog2.jar

%changelog
* Mon Jul 11 2022 Alexander Aring <aahringo@redhat.com> 1.2.8-1

- version 1.2.8.

* Mon Jul 11 2022 Alexander Aring <aahringo@redhat.com> 1.2.7-1

- Initial support.
- 
