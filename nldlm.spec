%global _vpath_srcdir %{name}-%{version}

Name:		nldlm
Version:	0.0.4
Release:	1%{?dist}
Summary:	Userspace software to act with dlm in the kernel via netlink

License:	GPL
URL:		https://gitlab.com/netcoder/nldlm/
Source0:	%{url}-/archive/%{version}/%{name}-%{version}.tar.gz

BuildRequires:	meson
BuildRequires:	gcc
BuildRequires:	libev-devel
BuildRequires:	lua-devel
Requires:	libev
Requires:	lua

%package devel
Summary:        Development libraries and header files for %{name}
Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel
%{summary} devel files.

%description
%{summary}.

%prep
%autosetup -c

%build
%meson
%meson_build

%install
%meson_install


#%files devel
#%{_includedir}/%{name}.h

%files
%{_libdir}/lib%{name}.so
%{_bindir}/%{name}d
%{_bindir}/%{name}cli
