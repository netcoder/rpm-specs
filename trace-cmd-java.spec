Name:		trace-cmd-java
Version:	0.0.2
Release:	1%{?dist}
Summary:	Bindings to parse Linux traces in Java

License:	GPLv2 and LGPLv2
URL:		https://gitlab.com/netcoder/trace-cmd-java
Source0:	https://gitlab.com/netcoder/trace-cmd-java/-/archive/%{version}/trace-cmd-java-%{version}.tar.bz2

BuildRequires:	gcc
BuildRequires:	make
BuildRequires:	java-17-openjdk-devel
BuildRequires:	swig
BuildRequires:	glibc-devel
BuildRequires:	coreutils
BuildRequires:	sed
BuildRequires:	pkgconfig

Requires:	java-17-openjdk

%description
libctracecmd provides Java bindings for libtracecmd to parse Linux traces.

%prep
%setup -q

%build
%set_build_flags
make -O -j1 V=1 VERBOSE=1 prefix=%{_prefix} libdir=%{_libdir} datadir=%{_datadir} all

%install
%make_install prefix=%{_prefix} libdir=%{_libdir} datadir=%{_datadir} install

%files
%{_libdir}/libctracecmdjava-%{version}.so
%{_libdir}/libctracecmdjava.so
%{_datadir}/java/tracecmd-%{version}.jar
%{_datadir}/java/tracecmd.jar

%changelog
* Fri Apr 15 2022 Alexander Aring <aahringo@redhat.com> - 0.0.1-1
	
- Initial support.
