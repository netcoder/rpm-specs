Name:           dlm2slog2
Version:        0.0.2
Release:        1%{?dist}
Summary:        takes dlm Linux traces and transfer it into slog2
License:        BSD
BuildArch:	noarch

URL:            https://gitlab.com/netcoder/dlm2slog2
Source0:        https://gitlab.com/netcoder/dlm2slog2/-/archive/%{version}/dlm2slog2-%{version}.tar.bz2

BuildRequires:  java-17-openjdk-devel
BuildRequires:  trace-cmd-java >= 0.0.2
BuildRequires:  slog2sdk
BuildRequires:	make
BuildRequires:	coreutils
BuildRequires:	sed

Requires:       java-17-openjdk
Requires:       trace-cmd-java >= 0.0.2
Requires:       slog2sdk

%description
This packages installs dlm2slog2 which is a graphical application to transfer
Linux dlm traces to slog2. The slog2 files can be viewed by jumpshot a
grapical viewer to see dlm states over time. See:

https://gitlab.com/netcoder/dlm2slog2/-/wikis/home

for more information.


%prep
%setup -q

%build
%set_build_flags
make -O -j1 prefix=%{_prefix} datadir=%{_datadir} all

%install
%make_install prefix=%{_prefix} datadir=%{_datadir} install

%files
%license LICENSES/BSD-3-Clause
%{_bindir}/dlm2slog2
%{_datadir}/java/dlm2slog2-%{version}.jar
%{_datadir}/java/dlm2slog2.jar

%changelog
* Mon Jul 11 2022 Alexander Aring <aahringo@redhat.com> 0.0.1-1

- Initial support.
